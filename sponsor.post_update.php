<?php

/**
 * @file
 * Post update functions for the Sponsor module.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\Core\Config\InstallStorage;
use Drupal\views\Entity\View;

/**
 * Adding sponsors_admin view
 */
function sponsor_post_update_add_sponsor_admin_view() {
  // Only create if the views module is enabled and the sponsors_admin view doesn't
  // exist.
  if (\Drupal::moduleHandler()->moduleExists('views')) {
    if (!View::load('sponsors_admin')) {
      // Save the sponsors_admin view to config.
      $module_handler = \Drupal::moduleHandler();
      $optional_install_path = $module_handler->getModule('sponsor')->getPath() . '/' . InstallStorage::CONFIG_OPTIONAL_DIRECTORY;
      $storage = new FileStorage($optional_install_path);

      \Drupal::entityTypeManager()
        ->getStorage('view')
        ->create($storage->read('views.view.sponsors_admin'))
        ->save();

      return t('The sponsors_admin view has been created.');
    }

    return t("The sponsors_admin view already exists and was not replaced.");
  }
}
